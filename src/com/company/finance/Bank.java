package com.company.finance;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

public class Bank extends Organization {
    private float mRate;
    private float commision;

    public Bank(String name, float mRate) {
        super(name);
        this.mRate = mRate;
        Map<String, Currency> rates = new HashMap<>();
        rates.put("usd", new Currency());
    }

    public float getRate() {
        return mRate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Bank bank = (Bank) o;
        return Float.compare(bank.mRate, mRate) == 0 &&
                Float.compare(bank.commision, commision) == 0;
    }

    @Override
    public int hashCode() {
        return Objects.hash(mRate, commision);
    }

    @Override
    public String toString() {
        return "Bank{" +
                "mRate=" + mRate +
                ", name='" + name + '\'' +
                '}';
    }

}
