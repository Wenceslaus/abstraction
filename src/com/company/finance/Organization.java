package com.company.finance;

public class Organization implements Comparable<Organization> {
    protected String name;

    public Organization(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    @Override
    public int compareTo(Organization o) {
        return name.toLowerCase().compareTo(o.getName().toLowerCase());
    }
}
