package com.company;

import com.company.finance.*;
import com.company.sorter.SortByName;

import java.util.*;

public class Main {

    public static void main(String[] args) {

        List<Bank> list = new ArrayList<>();
        list.add(new Bank("Privat", 27f));
        list.add(new Bank("Oshad", 27.5f));
        list.add(new Bank("PUMB", 27f));
        list.add(new Bank("Mono", 26.5f));
        Collections.sort(list);

        list.sort(new SortByName());

        Comparator<Bank> sorter = new Comparator<Bank>() {
            @Override
            public int compare(Bank o1, Bank o2) {
                return Float.compare(o1.getRate(), o2.getRate());
            }
        };

        list.sort((o1, o2) -> Float.compare(o1.getRate(), o2.getRate()));

        for (Bank bank : list) {
            System.out.println(bank);
        }
    }
}
