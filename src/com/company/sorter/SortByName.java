package com.company.sorter;

import com.company.finance.Bank;

import java.util.Comparator;

public class SortByName implements Comparator<Bank> {
    @Override
    public int compare(Bank o1, Bank o2) {
        return o1.getName().compareTo(o2.getName());
    }
}
