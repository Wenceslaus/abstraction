package com.company;

import java.util.*;

public class MainSet {
    public static void main(String[] args) {
        Set<String> set = new TreeSet<>();
        set.add("a");
        set.add("c");
        set.add("c");
        set.add("b");

        Iterator<String> iterator = set.iterator();
        while (iterator.hasNext()) {
            String element = iterator.next();
            System.out.println(element);
        }

        Map<String, String> map = new HashMap<>();
        map.put("abc", "123");
        String value = map.get("abc");
        Map.Entry<String, String> entry = map.entrySet().iterator().next();
        map.remove("abc");

    }
}
